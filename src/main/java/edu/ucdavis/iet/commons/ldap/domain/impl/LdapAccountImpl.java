package edu.ucdavis.iet.commons.ldap.domain.impl;

import java.util.Map;

import edu.ucdavis.iet.commons.ldap.domain.LdapAccount;
import edu.ucdavis.iet.commons.ldap.domain.util.LdapAccountConstants;

public class LdapAccountImpl implements LdapAccount
{
    private String uidNumber;
    private String uid;
    
    /**
     * Compose attributes from a Context Map 
     * @param ldapAccountContextMap
     */
    public LdapAccountImpl(Map<String,String> ldapAccountContextMap) {
            this.uidNumber = ldapAccountContextMap.get(LdapAccountConstants.USER_ID_NUMBER);           
            this.uid = ldapAccountContextMap.get(LdapAccountConstants.USER_ID);
    }
    
    /**
     * @see edu.ucdavis.iet.commons.ldap.domain.LdapAccount#getUid()
     */
    public String getUid()
    {
        return uid;
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.domain.LdapAccount#getUidNumber()
     */
    public String getUidNumber()
    {
        return uidNumber;
    }
    
	/**
	 * Constructs a String with all attributes in name = value format.
	 *
	 * @return a String representation of this object.
	 */
    // IETC-29: implement toString()
	public String toString() {
	    final String TAB = "\n";
	    
	    String retValue = "LdapMailListingDaoSpringImpl ( "
	        + "uidNumber = " + this.uidNumber + TAB
	        + "uid = " + this.uid + TAB
	        + " )";
	    return retValue;
	}

}
