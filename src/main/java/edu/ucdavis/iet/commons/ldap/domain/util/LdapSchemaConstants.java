package edu.ucdavis.iet.commons.ldap.domain.util;

/**
 * This class maps human readable attributes to LDAP schema attributes.
 *  
 * @author eldavid
 * 
 */
public class LdapSchemaConstants
{
    /**
     * Organizational Unit
     */
    public static final String LDAP_OU = "ou";

    /**
     * Common Name
     */
    public static final String LDAP_CN = "cn";
    
    /**
     * Domain Component
     */
    public static final String LDAP_DC = "dc";
    
    /**
     * People Schema Name
     */
    public static final String PEOPLE_SCHEMA_NAME = "People";
    
    /**
     * Listings Schema Name
     */
    public static final String LISTINGS_SCHEMA_NAME = "Listings";

    /**
     * Accounts Schema Name
     */
    public static final String ACCOUNTS_SCHEMA_NAME = "Accounts";
    
    /**
     * Object Class Attribute Name
     */
    public static final String OBJECT_CLASS = "objectClass";
}
