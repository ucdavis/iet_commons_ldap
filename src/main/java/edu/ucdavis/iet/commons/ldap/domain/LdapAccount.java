package edu.ucdavis.iet.commons.ldap.domain;

/**
 * This class represents a single entry in the ou=Accounts schema in the UC Davis LDAP Directory.
 */

public interface LdapAccount {

	/**
	 *  User ID Number - also known as Principal ID or UNIX ID
	 */
	public String getUidNumber();
	
	/**
	 *  User ID - also known as User Account or Principal Name
	 */
	public String getUid();
	
}
