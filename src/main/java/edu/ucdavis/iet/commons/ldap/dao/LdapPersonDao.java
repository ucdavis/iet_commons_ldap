package edu.ucdavis.iet.commons.ldap.dao;

import java.util.List;
import java.util.Map;

import edu.ucdavis.iet.commons.ldap.domain.LdapPerson;

/**
 * Data Access Object for fetching records of persons from LDAP. 
 * 
 * @author eldavid
 *
 */
public interface LdapPersonDao {

	/**
	 * Find an LDAP Person by a Universal User ID
	 * 
	 * @param uuid	Universal User ID - Uniquely identifies each record in LDAP
	 * @return		An LDAP Person
	 */
	public LdapPerson findByUniversalUserId(String uuid);

	/**
	 * Find one or more LDAP Persons by one or more Universal User IDs
	 * 
	 * @param uuids	List of Universal User IDs
	 * @return		A list of LDAP Persons
	 */
	public List<LdapPerson> findByUniversalUserId(List<String> uuids);
	
	/**
	 * Find an LDAP Person by User ID
	 * 
	 * @param uid	User ID - Kerberos ID
	 * @return		An LDAP Person
	 */
	public LdapPerson findByUserId(String uid);
		
	/**
	 * Find one or more LDAP Persons by Home Department Number
	 * 
	 * @param departmentNumber
	 * @return	An LDAP Person
	 */
	public List<LdapPerson> findByDepartmentId(String departmentNumber);
	
	/**
	 * Search for one or more LDAP Persons by one or more criteria. The method treats
	 * the criteria as exact criteria.
	 * 
	 * @param searchCriteria	Attributes to search by, in the form of key-value pairs
	 * @return					A list of LDAP Persons 
	 */
	public List<LdapPerson> findByCriteria(Map<String,String> searchCriteria);
	
	/**
	 * Search for one or more LDAP Persons by one or more criteria, specifying what type
	 * of search should be performed (e.g. exact, wildcard, like, etc). 
	 * See LdapSearchType for details.
	 * 
	 * @param searchCriteria	Attributes to search by, in the form of key-value pairs
	 * @param searchType		Determine how the searchCriteria should be filtered by LDAP search
	 * @return					A list of LDAP Persons 
	 */
	public List<LdapPerson> findByCriteria(Map<String,String> searchCriteria, LdapSearchType searchType);	
}
