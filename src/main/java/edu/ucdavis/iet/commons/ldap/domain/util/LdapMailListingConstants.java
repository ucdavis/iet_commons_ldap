package edu.ucdavis.iet.commons.ldap.domain.util;

/**
 * This class extends LDAP Listing object attributes to LDAP Mail Listing object
 * attributes. It is helpful for mapping attributes between LDAP Mail Listing
 * records and external system records.
 * 
 * @author eldavid
 * 
 */
public class LdapMailListingConstants extends LdapListingConstants
{

    /**
     * E-mail Address
     */
    public static final String EMAIL_ADDRESS = "mail";

}
