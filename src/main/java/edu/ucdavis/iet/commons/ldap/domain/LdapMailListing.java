package edu.ucdavis.iet.commons.ldap.domain;

/**
 * This class represents a single e-mail listing in the ou=Listings schema in the UC Davis LDAP Directory.
 */

public interface LdapMailListing extends LdapListing {
	
	/**
	 * Primary ID for Mail Listing
	 */
	public String getUcdUriOid();
	
	/**
	 * E-mail Address
	 */
	public String getMail();
}
