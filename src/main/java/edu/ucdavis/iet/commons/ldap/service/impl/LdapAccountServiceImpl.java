package edu.ucdavis.iet.commons.ldap.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import edu.ucdavis.iet.commons.ldap.dao.LdapAccountDao;
import edu.ucdavis.iet.commons.ldap.domain.LdapAccount;
import edu.ucdavis.iet.commons.ldap.service.LdapAccountService;

public class LdapAccountServiceImpl implements LdapAccountService
{

    /*
     * Injected LDAP Account Data Access Object.
     */
    private LdapAccountDao ldapAccountDao;
    public void setLdapAccountDao(LdapAccountDao ldapAccountDao)
    {
        this.ldapAccountDao = ldapAccountDao;
    }
    
    /**
     * @see edu.ucdavis.iet.commons.ldap.service.LdapAccountService#findAccountsByUserID(java.lang.String)
     */
    @Override
    public List<LdapAccount> findAccountsByUserID(String uid)
    {
        if (StringUtils.isBlank(uid))
        {
            throw new IllegalArgumentException("User ID is blank or null.");
        }
        return ldapAccountDao.findAccountsByUserID(uid);
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.service.LdapAccountService#findAccountsByUserIDNumber(java.lang.String)
     */
    @Override
    public List<LdapAccount> findAccountsByUserIDNumber(String uidNumber)
    {
        if (StringUtils.isBlank(uidNumber))
        {
            throw new IllegalArgumentException("User ID Number is blank or null.");
        }
        return ldapAccountDao.findAccountsByUserIDNumber(uidNumber);
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.service.LdapAccountService#findByUserId(java.lang.String, java.lang.String)
     */
    @Override
    public LdapAccount findByUserId(String uid, String serviceName)
    {
        if (StringUtils.isBlank(uid))
        {
            throw new IllegalArgumentException("User ID is blank or null.");
        }
        if (StringUtils.isBlank(serviceName))
        {
            throw new IllegalArgumentException("Service Name is blank or null.");
        }        
        return ldapAccountDao.findByUserId(uid, serviceName);
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.service.LdapAccountService#findByUserIdNumber(java.lang.String, java.lang.String)
     */
    @Override
    public LdapAccount findByUserIdNumber(String uidNumber, String serviceName)
    {
        if (StringUtils.isBlank(uidNumber))
        {
            throw new IllegalArgumentException("User ID Number is blank or null.");
        }
        if (StringUtils.isBlank(serviceName))
        {
            throw new IllegalArgumentException("Service Name is blank or null.");
        }  
        return ldapAccountDao.findByUserIdNumber(uidNumber, serviceName);
    }
}
