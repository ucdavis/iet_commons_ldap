package edu.ucdavis.iet.commons.ldap.domain.util;

/**
 * This class maps human readable attributes to LDAP Account object attributes.
 * It is helpful for mapping attributes between LDAP Account records and
 * external system records.
 * 
 * @author eldavid
 * 
 */
public class LdapAccountConstants
{

    /**
     * @see edu.ucdavis.iet.commons.ldap.domain.LdapAccount#getUid()
     */
    public static final String USER_ID_NUMBER = "uidNumber";

    /**
     * @see edu.ucdavis.iet.commons.ldap.domain.LdapAccount#getUidNumber()
     */
    public static final String USER_ID = "uid";
    
    /**
     * Service Provision Name
     */
    public static final String SERVICE_NAME = "ou";
}
