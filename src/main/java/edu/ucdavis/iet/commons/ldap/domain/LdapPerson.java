package edu.ucdavis.iet.commons.ldap.domain;

import java.util.List;

/**
 * This class represents a single entry in the ou=People schema in the UC Davis LDAP Directory.
 */

public interface LdapPerson {
	
	/**
	 * Country
	 */
	public String getC();

	/**
	 *  dn - the primary key of an LDAP entry also known as distinguished Name
	 */
	public String getDn();

	/**
	 *  Universal User ID - also known as Entity ID or Mothra ID
	 */
	public String getUcdPersonUUID();

	/**
	 *  User ID - also known as User Account or Principal Name
	 */
	public List<String> getUid();

	/**
	 * Formatted "White Pages" Name
	 */
	public String getDisplayName();

	/**
	 * E-mail Address
	 */
	public List<String> getMail();

	/**
	 * Phone Number
	 */
	public List<String> getTelephoneNumber();

	/**
	 * Street
	 */
	public List<String> getStreet();

	/**
	 * City or Locale
	 */
	public List<String> getL();

	/**
	 * State
	 */
	public List<String> getSt();

	/**
	 * Postal Code - also known as ZIP Code
	 */
	public List<String> getPostalCode();

	/**
	 * Composition of Street, City, State, and Postal Code
	 */
	public List<String> getPostalAddress();

	/**
	 * Last Name
	 */
	public List<String> getSn();

	/**
	 * Nickname
	 */
	public String getEduPersonNickName();

	/**
	 * First Name
	 */
	public String getGivenName();

	/**
	 * Full Name - also known as Common Name
	 */
	public List<String> getCn();

	/**
	 * Affiliation(s) with UCD
	 */
	public List<String> getEduPersonAffiliation();

	/**
	 * Affiliation(s) with UCD - An augmented form of eduPersonAffiliation
	 */
	public List<String> getUcdPersonAffiliation();

	/**
	 * Home Department Number - also known as Primary Department Number
	 */
	public List<String> getDepartmentNumber();

	/**
	 * Home Department Name
	 */
	public List<String> getOu();
	
	/**
	 * Pager number
	 */
	public String getPager();

	/**
	 * Title - also known as Job Title or Position Title
	 */
	public List<String> getTitle();

	/**
	 * Employee ID - also known as Employee Number
	 */
	public String getEmployeeNumber();

	/**
	 * get the ucdPublishItemFlag values to break into the various fields that follow
	 */
	public List<String> getUcdPublishItemFlag();

	/**
	 * UCD Publish Flag for all attributes
	 */
	public String getUcdPublishPerson();

	/**
	 * UCD Publish Flag for Title attribute
	 */
	public String getUcdPublishTitle();

	/**
	 * UCD Publish Flag for Department attribute
	 */
	public String getUcdPublishDepartment();

	/**
	 * UCD Publish Flag for Address attributes (including postalAddress, street, locality, state, postalCode,
	 *     country, roomNumber, and physical Delivery Office Name
	 */
	public String getUcdPublishAddress();

	/**
	 * UCD Publish Flag for Phone attribute
	 */
	public String getUcdPublishPhone();

	/**
	 * UCD Publish Flag for Mobile Phone attribute
	 */
	public String getUcdPublishMobile();

	/**
	 * UCD Publish Flag for Fax attribute
	 */
	public String getUcdPublishFax();

	/**
	 * UCD Publish Flag for Mail attribute
	 */
	public String getUcdPublishMail();

	/**
	 * UCD Publish Flag for Labeled Uri attribute
	 */
	public String getUcdPublishLabeledUri();

}
