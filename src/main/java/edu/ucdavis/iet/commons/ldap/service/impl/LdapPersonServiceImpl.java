package edu.ucdavis.iet.commons.ldap.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.ldap.NameNotFoundException;

import edu.ucdavis.iet.commons.ldap.domain.LdapPerson;
import edu.ucdavis.iet.commons.ldap.service.LdapPersonService;
import edu.ucdavis.iet.commons.ldap.dao.LdapPersonDao;
import edu.ucdavis.iet.commons.ldap.dao.LdapSearchType;

/**
 * Base implementation of the LDAP Person Service
 * 
 * @author eldavid
 * 
 */
public class LdapPersonServiceImpl implements LdapPersonService
{
    /**
     * Injected LDAP Person Data Access Object
     */
    private LdapPersonDao ldapPersonDao;

    public void setLdapPersonDao(LdapPersonDao ldapPersonDao)
    {
        this.ldapPersonDao = ldapPersonDao;
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.service.LdapPersonService#findByDepartmentId(String)
     */
    @Override
    public List<LdapPerson> findByDepartmentId(String departmentNumber)
    {
        if (StringUtils.isBlank(departmentNumber))
        {
            throw new IllegalArgumentException("Department Number is blank or null.");
        }
        return ldapPersonDao.findByDepartmentId(departmentNumber);
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.service.LdapPersonService#findByUniversalUserId(String)
     */
    @Override
    public LdapPerson findByUniversalUserId(String uuid)
    {
        if (StringUtils.isBlank(uuid))
        {
            throw new IllegalArgumentException("Universal User ID is blank or null.");
        }
        return ldapPersonDao.findByUniversalUserId(uuid);
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.service.LdapPersonService#findByUniversalUserId(List)
     */
    @Override
    public List<LdapPerson> findByUniversalUserId(List<String> uuids)
    {
        if (uuids.isEmpty() || uuids == null)
        {
            throw new IllegalArgumentException("List of Universal User IDs is empty or null.");
        }
        return ldapPersonDao.findByUniversalUserId(uuids);
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.service.LdapPersonService#findByUserId(String)
     */
    @Override
    public LdapPerson findByUserId(String uid)
    {
        if (StringUtils.isBlank(uid))
        {
            throw new IllegalArgumentException("User ID is blank or null.");
        }
        try
        {
            return ldapPersonDao.findByUserId(uid);
        }
        catch (EmptyResultDataAccessException erdae)
        {
            throw new NameNotFoundException("Trying to retrieve User ID: " + uid);
        }
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.service.LdapPersonService#findByCriteria(Map, boolean)
     */
    @Override
    public List<LdapPerson> findByCriteria(Map<String, String> searchCriteria, boolean exactMatch)
    {
        if (searchCriteria == null || searchCriteria.isEmpty())
        {
            throw new IllegalArgumentException("Search criteria are empty or null.");
        }
        return this.findByCriteria(searchCriteria, exactMatch ? LdapSearchType.Equals : LdapSearchType.WhitespaceWildcards);
    }
    
    /**
     * @see edu.ucdavis.iet.commons.ldap.service.LdapPersonService#findByCriteria(Map)
     */
    @Override
    public List<LdapPerson> findByCriteria(Map<String, String> searchCriteria)
    {
        if (searchCriteria == null || searchCriteria.isEmpty())
        {
            throw new IllegalArgumentException("Search criteria are empty or null.");
        }
        return this.findByCriteria(searchCriteria, LdapSearchType.Equals);
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.service.LdapPersonService#findByCriteria(Map, LdapSearchType)
     */
	@Override
	public List<LdapPerson> findByCriteria(Map<String, String> searchCriteria, LdapSearchType searchType) 
	{
        if (searchCriteria == null || searchCriteria.isEmpty())
        {
            throw new IllegalArgumentException("Search criteria are empty or null.");
        }
        return ldapPersonDao.findByCriteria(searchCriteria, searchType);
	}
}
