package edu.ucdavis.iet.commons.ldap.service;

import java.util.List;
import java.util.Map;

import edu.ucdavis.iet.commons.ldap.dao.LdapSearchType;
import edu.ucdavis.iet.commons.ldap.domain.LdapMailListing;

/**
 * IET LDAP Mail Listing Service API for fetching records of E-mail Listings
 * from LDAP.
 * 
 * @author eldavid
 * 
 */
public interface LdapMailListingService
{

    /**
     * Find one or more E-mail Listings by a Person's Universal User ID
     * 
     * @param uuid
     *            Universal User ID - Uniquely identifies each record in LDAP
     * @return An LDAP Mail Listing
     */
    public List<LdapMailListing> findMailListings(String uuid);

    /**
     * Find the primary E-mail Listing by a Person's Universal User ID
     * 
     * @param uuid
     *            Universal User ID - Uniquely identifies each record in LDAP
     * @return An LDAP Mail Listing
     */
    public LdapMailListing findPrimaryMailListing(String uuid);

    /**
     * Search for one or more LDAP Mail Listings by one or more criteria
     * 
     * @param searchCriteria
     *            Attributes to search by, in the form of key-value pairs
     * @return A list of LDAP Mail Listings
     */
    public List<LdapMailListing> findByCriteria(Map<String, String> searchCriteria);

	/**
	 * Search for one or more LDAP Mail listings by one or more criteria, specifying what type
	 * of search should be performed (e.g. exact, wildcard, like, etc). 
	 * See LdapSearchType for details.
	 * 
	 * @param searchCriteria	Attributes to search by, in the form of key-value pairs
	 * @param searchType		Determine how the searchCriteria should be filtered by LDAP search
	 * @return					A list of LDAP Mail Listings 
	 */
	public List<LdapMailListing> findByCriteria(Map<String,String> searchCriteria, LdapSearchType searchType);   
	
    /**
     * Search for one or more LDAP Mail Listings by e-mail address
     * 
     * @param mail
     *            The e-mail address to search by
     * @return A list of LDAP Mail Listings
     */
    public List<LdapMailListing> findByEmailAddress(String mail);
}
