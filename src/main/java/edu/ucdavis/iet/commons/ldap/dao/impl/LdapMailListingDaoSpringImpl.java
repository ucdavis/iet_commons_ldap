package edu.ucdavis.iet.commons.ldap.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.ldap.NameNotFoundException;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.AbstractContextMapper;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.filter.LikeFilter;
import org.springframework.ldap.filter.WhitespaceWildcardsFilter;

import edu.ucdavis.iet.commons.ldap.dao.LdapMailListingDao;
import edu.ucdavis.iet.commons.ldap.dao.LdapSearchType;
import edu.ucdavis.iet.commons.ldap.domain.LdapMailListing;
import edu.ucdavis.iet.commons.ldap.domain.impl.LdapMailListingImpl;
import edu.ucdavis.iet.commons.ldap.domain.util.LdapMailListingConstants;
import edu.ucdavis.iet.commons.ldap.domain.util.LdapSchemaConstants;

/**
 * Spring LDAP implementation of LdapMailListingDAO
 * 
 * @author eldavid
 * 
 */
public class LdapMailListingDaoSpringImpl implements LdapMailListingDao
{

    /**
     * @see edu.ucdavis.iet.commons.ldap.dao.LdapMailListingDao#findMailListings(String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<LdapMailListing> findMailListings(String uuid)
    {
        if (StringUtils.isBlank(uuid))
        {
            throw new IllegalArgumentException("Universal User ID is blank or null.");
        }
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter("ucdPersonUUID", uuid)).and(new WhitespaceWildcardsFilter("mail", " "));
        DistinguishedName dn = new DistinguishedName();
        dn.add(LdapSchemaConstants.LDAP_OU, LdapSchemaConstants.LISTINGS_SCHEMA_NAME);
        List<LdapMailListing> ldapMailListings = (List<LdapMailListing>) ldapTemplate.search(dn, filter.encode(),
                getContextMapper());
        return ldapMailListings;
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.dao.LdapMailListingDao#findPrimaryMailListing(String)
     */
    @Override
    public LdapMailListing findPrimaryMailListing(String uuid)
    {
        if (StringUtils.isBlank(uuid))
        {
            throw new IllegalArgumentException("Universal User ID is blank or null.");
        }
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter("ucdPersonUUID", uuid)).and(new EqualsFilter("ucdListingOrder", 1)).and(
                new EqualsFilter("ucdDisplayOrder", 1)).and(new WhitespaceWildcardsFilter("mail", " "));
        DistinguishedName dn = new DistinguishedName();
        dn.add(LdapSchemaConstants.LDAP_OU, LdapSchemaConstants.LISTINGS_SCHEMA_NAME);
        LdapMailListing ldapMailListing = (LdapMailListing) ldapTemplate.searchForObject(dn, filter.encode(),
                getContextMapper());
        return ldapMailListing;
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.dao.LdapMailListingDao#findByCriteria(Map)
     */
    @Override
    public List<LdapMailListing> findByCriteria(Map<String, String> searchCriteria)
    {
        return findByCriteria(searchCriteria, LdapSearchType.Equals);
    }   
    
    /**
     * @see edu.ucdavis.iet.commons.ldap.dao.LdapMailListingDao#findByCriteria(Map, LdapSearchType)
     */
    @SuppressWarnings("unchecked")
	@Override
	public List<LdapMailListing> findByCriteria(
			Map<String, String> searchCriteria, LdapSearchType searchType)
	{
        if (searchCriteria == null || searchCriteria.isEmpty())
        {
            throw new IllegalArgumentException("Search criteria are empty or null.");
        }
        List<LdapMailListing> ldapMailListingList = new ArrayList<LdapMailListing>();
        // Initialize search filter
        AndFilter andFilter = new AndFilter();
        // Iterate through the search criteria and add to the search filter
        switch (searchType)
        {
	        case Equals:
	            for (String key : searchCriteria.keySet())
	            {
	                // Use the EqualsFilter for Equal types
	                andFilter.and(new EqualsFilter(key, searchCriteria.get(key)));
	            }
	            break;
	        case WhitespaceWildcards :
	            for (String key : searchCriteria.keySet())
	            {
	                // Use the WhitespaceWildcardsFilter for WhitespaceWildcards types
	                andFilter.and(new WhitespaceWildcardsFilter(key, searchCriteria.get(key)));
	            }
	            break;
	        case Like :
	            for (String key : searchCriteria.keySet())
	            {
	                // Use the LikeFilter for Like types
	                andFilter.and(new LikeFilter(key, searchCriteria.get(key)));
	            }
	            break;	        
            default:
	        	throw new UnsupportedOperationException("Unsupported Ldap Search Type: " + searchType);
        }        
        DistinguishedName dn = new DistinguishedName();
        dn.add(LdapSchemaConstants.LDAP_OU, LdapSchemaConstants.LISTINGS_SCHEMA_NAME);
        ldapMailListingList = ldapTemplate.search(dn, andFilter.encode(), getContextMapper());
        if (ldapMailListingList.isEmpty())
        {
            throw new NameNotFoundException("Search produced no results.");
        }
        return ldapMailListingList;  
    }
    
    /**
     * Injected Spring LDAP Template
     * 
     * @param ldapTemplate
     *            Spring LDAP Template
     */
    private LdapTemplate ldapTemplate;

    public void setLdapTemplate(LdapTemplate ldapTemplate)
    {
        this.ldapTemplate = ldapTemplate;
    }

    /**
     * Get a context mapper
     * 
     * @return LDAP Mail Listing Context Mapper
     */
    private ContextMapper getContextMapper()
    {
        return new LdapMailListingContextMapper();
    }

    /**
     * Map LDAP Listing attributes to LDAPMailListing object
     * 
     * @author eldavid
     * 
     */
    private static class LdapMailListingContextMapper extends AbstractContextMapper
    {
        protected Object doMapFromContext(DirContextOperations context)
        {
            Map<String, String> ldapMailListingAttributeMap = new HashMap<String, String>(7);
            ldapMailListingAttributeMap.put(LdapMailListingConstants.URI_OID, context.getStringAttribute(LdapMailListingConstants.URI_OID));
            ldapMailListingAttributeMap.put(LdapMailListingConstants.UNIVERSAL_USER_ID, context.getStringAttribute(LdapMailListingConstants.UNIVERSAL_USER_ID));
            ldapMailListingAttributeMap.put(LdapMailListingConstants.EMAIL_ADDRESS, context.getStringAttribute(LdapMailListingConstants.EMAIL_ADDRESS));
            ldapMailListingAttributeMap.put(LdapMailListingConstants.DISPLAY_ORDER, context.getStringAttribute(LdapMailListingConstants.DISPLAY_ORDER));
            ldapMailListingAttributeMap.put(LdapMailListingConstants.GROUPING_ORDER, context.getStringAttribute(LdapMailListingConstants.GROUPING_ORDER));
            ldapMailListingAttributeMap.put(LdapMailListingConstants.LISTING_ORDER, context.getStringAttribute(LdapMailListingConstants.LISTING_ORDER));
            ldapMailListingAttributeMap.put(LdapMailListingConstants.PUBLISH_FLAG, context.getStringAttribute(LdapMailListingConstants.PUBLISH_FLAG));
            LdapMailListing ldapMailListing = new LdapMailListingImpl(ldapMailListingAttributeMap);
            return ldapMailListing;
        }
    }


}
