package edu.ucdavis.iet.commons.ldap.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.ldap.NameNotFoundException;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.AbstractContextMapper;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.filter.LikeFilter;
import org.springframework.ldap.filter.WhitespaceWildcardsFilter;

import edu.ucdavis.iet.commons.ldap.dao.LdapPersonDao;
import edu.ucdavis.iet.commons.ldap.dao.LdapSearchType;
import edu.ucdavis.iet.commons.ldap.domain.LdapPerson;
import edu.ucdavis.iet.commons.ldap.domain.impl.LdapPersonImpl;
import edu.ucdavis.iet.commons.ldap.domain.util.LdapPersonConstants;
import edu.ucdavis.iet.commons.ldap.domain.util.LdapSchemaConstants;

/**
 * Spring LDAP implementation of LdapPersonDAO
 *
 * @author eldavid
 *
 */
public class LdapPersonDaoSpringImpl implements LdapPersonDao
{

    /**
     * @see edu.ucdavis.iet.commons.ldap.dao.LdapPersonDao#findByUniversalUserId(String)
     */
    public LdapPerson findByUniversalUserId(String uuid)
    {
        if (StringUtils.isBlank(uuid))
        {
            throw new IllegalArgumentException("Universal User ID is blank or null.");
        }
        DistinguishedName dn = new DistinguishedName();
        dn.add(LdapSchemaConstants.LDAP_OU, LdapSchemaConstants.PEOPLE_SCHEMA_NAME);
        dn.add(LdapPersonConstants.UNIVERSAL_USER_ID,uuid);
        LdapPerson ldapPerson = (LdapPerson)ldapTemplate.lookup(dn, getContextMapper());
        return ldapPerson;
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.dao.LdapPersonDao#findByUniversalUserId(List)
     */
    @SuppressWarnings("unchecked")
    public List<LdapPerson> findByUniversalUserId(List<String> uuids)
    {
        if (uuids == null || uuids.isEmpty())
        {
            throw new IllegalArgumentException("List of Universal User IDs is empty or null.");
        }
        // Initialize list
        List ldapPersonList = new ArrayList(uuids.size());
        // Iterate through the list and query for each Universal User ID
        for (String uuid : uuids)
        {
            try
            {
                ldapPersonList.add(findByUniversalUserId(uuid));
            }
            catch (NameNotFoundException e)
            {
                // Keep going if the LDAP Person is not found
            }
        }
        return ldapPersonList;
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.dao.LdapPersonDao#findByUserId(String)
     */
    public LdapPerson findByUserId(String uid)
    {
        if (StringUtils.isBlank(uid))
        {
            throw new IllegalArgumentException("User ID is blank or null.");
        }
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter("objectclass", "person")).and(new EqualsFilter("uid", uid));
        DistinguishedName dn = new DistinguishedName();
        dn.add(LdapSchemaConstants.LDAP_OU, LdapSchemaConstants.PEOPLE_SCHEMA_NAME);
        try
        {
            LdapPerson ldapPerson = (LdapPerson) ldapTemplate.searchForObject(dn,filter.encode(),getContextMapper());
            return ldapPerson;
        }
        catch (NameNotFoundException e)
        {
            // Return nothing if the LDAP Person is not found
            return null;
        }
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.dao.LdapPersonDao#findByDepartmentId(String)
     */
    public List<LdapPerson> findByDepartmentId(String departmentNumber)
    {
        if (StringUtils.isBlank(departmentNumber))
        {
            throw new IllegalArgumentException("Department Number is blank or null.");
        }
        Map<String, String> departmentCriterion = new HashMap<String, String>();
        departmentCriterion.put(LdapPersonConstants.PRIMARY_DEPARTMENT_CODE, departmentNumber);
        return findByCriteria(departmentCriterion);
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.dao.LdapPersonDao#findByCriteria(Map)
     */
    public List<LdapPerson> findByCriteria(Map<String, String> searchCriteria)
    {
        return findByCriteria(searchCriteria, LdapSearchType.Equals);
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.dao.LdapPersonDao#findByCriteria(Map, LdapSearchType)
     */
	@Override
    @SuppressWarnings("unchecked")
	public List<LdapPerson> findByCriteria(Map<String, String> searchCriteria, LdapSearchType searchType) 
	{
        if (searchCriteria == null || searchCriteria.isEmpty())
        {
            throw new IllegalArgumentException("Search criteria are empty or null.");
        }
        List<LdapPerson> ldapPersonList = new ArrayList<LdapPerson>();
        // If a Universal User ID key is found in the search criteria, use
        // findByUniversalUserId()
        if (searchCriteria.containsKey(LdapPersonConstants.UNIVERSAL_USER_ID))
        {
            ldapPersonList.add(findByUniversalUserId(searchCriteria.get(LdapPersonConstants.UNIVERSAL_USER_ID)));
            return ldapPersonList;
        }
        // If a User ID key is found in the search criteria, use findByUserId()
        if (searchCriteria.containsKey(LdapPersonConstants.USER_ID))
        {
            ldapPersonList.add(findByUserId(searchCriteria.get(LdapPersonConstants.USER_ID)));
            return ldapPersonList;
        }
        // Initialize search filter
        AndFilter andFilter = new AndFilter();
        andFilter.and(new EqualsFilter("objectclass", "person"));
        // Iterate through the search criteria and add to the search filter
        switch (searchType)
        {
	        case Equals:
	            for (String key : searchCriteria.keySet())
	            {
	                // Use the EqualsFilter for Equal types
	                andFilter.and(new EqualsFilter(key, searchCriteria.get(key)));
	            }
	            break;
	        case WhitespaceWildcards :
	            for (String key : searchCriteria.keySet())
	            {
	                // Use the WhitespaceWildcardsFilter for WhitespaceWildcards types
	                andFilter.and(new WhitespaceWildcardsFilter(key, searchCriteria.get(key)));
	            }
	            break;
	        case Like :
	            for (String key : searchCriteria.keySet())
	            {
	                // Use the LikeFilter for Like types
	                andFilter.and(new LikeFilter(key, searchCriteria.get(key)));
	            }
	            break;	        
            default:
	        	throw new UnsupportedOperationException("Unsupported Ldap Search Type: " + searchType);
        }	
        DistinguishedName dn = new DistinguishedName();
        dn.add(LdapSchemaConstants.LDAP_OU, LdapSchemaConstants.PEOPLE_SCHEMA_NAME);
        ldapPersonList = ldapTemplate.search(dn, andFilter.encode(), getContextMapper());
        if (ldapPersonList.isEmpty())
        {
            throw new NameNotFoundException("Search produced no results.");
        }
        return ldapPersonList;
	}    

    /**
     * Spring LDAP Template injected by Spring Framework
     *
     */
    private LdapTemplate ldapTemplate;

    /**
     * Inject Spring LDAP Template
     *
     * @param ldapTemplate
     *            Spring LDAP Template
     */
    public void setLdapTemplate(LdapTemplate ldapTemplate)
    {
        this.ldapTemplate = ldapTemplate;
    }

    /**
     * Get a context mapper
     *
     * @return LDAP Person Context Mapper
     */
    private ContextMapper getContextMapper()
    {
        return new LdapPersonContextMapper();
    }

    /**
     * Map LDAP Person attributes to LDAPPerson object
     *
     * @author eldavid
     *
     */
    private static class LdapPersonContextMapper extends AbstractContextMapper
    {
        @Override
        protected Object doMapFromContext(DirContextOperations context)
        {
            LdapPersonImpl ldapPerson = new LdapPersonImpl();
            ldapPerson.setC(context.getStringAttribute(LdapPersonConstants.COUNTRY));            
            ldapPerson.setUcdPersonUUID(context.getStringAttribute(LdapPersonConstants.UNIVERSAL_USER_ID));
            ldapPerson.setUid(context.getStringAttributes(LdapPersonConstants.USER_ID));
            ldapPerson.setTitle(context.getStringAttributes(LdapPersonConstants.EMPLOYEE_TITLE));
            ldapPerson.setCn(context.getStringAttributes(LdapPersonConstants.FULL_NAME));
            ldapPerson.setSn(context.getStringAttributes(LdapPersonConstants.LAST_NAME));
            ldapPerson.setMail(context.getStringAttributes(LdapPersonConstants.EMAIL_ADDRESS));
            ldapPerson.setDisplayName(context.getStringAttribute(LdapPersonConstants.DISPLAY_NAME));
            ldapPerson.setTelephoneNumber(context.getStringAttributes(LdapPersonConstants.PHONE_NUMBER));
            ldapPerson.setPager(context.getStringAttribute(LdapPersonConstants.PAGER));
            ldapPerson.setStreet(context.getStringAttributes(LdapPersonConstants.STREET));
            ldapPerson.setL(context.getStringAttributes(LdapPersonConstants.CITY_NAME));
            ldapPerson.setSt(context.getStringAttributes(LdapPersonConstants.STATE_CODE));
            ldapPerson.setPostalCode(context.getStringAttributes(LdapPersonConstants.POSTAL_CODE));
            ldapPerson.setPostalAddress(context.getStringAttributes(LdapPersonConstants.POSTAL_ADDRESS));
            ldapPerson.setGivenName(context.getStringAttribute(LdapPersonConstants.FIRST_NAME));
            ldapPerson.setEduPersonNickName(context.getStringAttribute(LdapPersonConstants.NICK_NAME));
            ldapPerson.setEduPersonAffiliation(context.getStringAttributes(LdapPersonConstants.AFFILIATION_TYPE_CODE));
            ldapPerson.setUcdPersonAffiliation(context.getStringAttributes(LdapPersonConstants.UCD_AFFILIATION_TYPE_CODE));
            ldapPerson.setDn(context.getDn().toString());
            ldapPerson.setDepartmentNumber(context.getStringAttributes(LdapPersonConstants.PRIMARY_DEPARTMENT_CODE));
            ldapPerson.setOu(context.getStringAttributes(LdapPersonConstants.PRIMARY_DEPARTMENT_NAME));
            ldapPerson.setEmployeeNumber(context.getStringAttribute(LdapPersonConstants.EMPLOYEE_ID));
            ldapPerson.setUcdPublishItemFlag(context.getStringAttributes(LdapPersonConstants.UCD_PUBLISH_FLAGS));
            // TODO IETC-33
            // Test if any UCD Publish Item Flags were returned before parsing to avoid NullPointerExceptions
            if (ldapPerson.getUcdPublishItemFlag() != null)
            {
	            for (String publish : ldapPerson.getUcdPublishItemFlag())
	            {
	                String [] publishValues = publish.split(":");
	                if (publishValues[0].equalsIgnoreCase(LdapPersonConstants.UCD_PUBLISH_FLAG_ALL_ATTR))
	                {
	                    ldapPerson.setUcdPublishPerson(publishValues[1]);
	                }
	                else if (publishValues[0].equalsIgnoreCase(LdapPersonConstants.UCD_PUBLISH_TITLE))
	                {
	                    ldapPerson.setUcdPublishTitle(publishValues[1]);
	                }
	                else if (publishValues[0].equalsIgnoreCase(LdapPersonConstants.UCD_PUBLISH_DEPARTMENT))
	                {
	                    ldapPerson.setUcdPublishDepartment(publishValues[1]);
	                }
	                else if (publishValues[0].equalsIgnoreCase(LdapPersonConstants.UCD_PUBLISH_ADDRESS))
	                {
	                    ldapPerson.setUcdPublishAddress(publishValues[1]);
	                }
	                else if (publishValues[0].equalsIgnoreCase(LdapPersonConstants.UCD_PUBLISH_PHONE))
	                {
	                    ldapPerson.setUcdPublishPhone(publishValues[1]);
	                }
	                else if (publishValues[0].equalsIgnoreCase(LdapPersonConstants.UCD_PUBLISH_MOBILE))
	                {
	                    ldapPerson.setUcdPublishMobile(publishValues[1]);
	                }
	                else if (publishValues[0].equalsIgnoreCase(LdapPersonConstants.UCD_PUBLISH_FAX))
	                {
	                    ldapPerson.setUcdPublishFax(publishValues[1]);
	                }
	                else if (publishValues[0].equalsIgnoreCase(LdapPersonConstants.UCD_PUBLISH_MAIL))
	                {
	                    ldapPerson.setUcdPublishMail(publishValues[1]);
	                }
	                else if (publishValues[0].equalsIgnoreCase(LdapPersonConstants.UCD_PUBLISH_URI))
	                {
	                    ldapPerson.setUcdPublishLabeledUri(publishValues[1]);
	                }
	            }
            }
            return ldapPerson;
        }
    }


}
