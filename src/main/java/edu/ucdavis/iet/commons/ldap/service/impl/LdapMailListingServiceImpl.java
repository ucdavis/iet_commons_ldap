package edu.ucdavis.iet.commons.ldap.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.EmptyResultDataAccessException;

import edu.ucdavis.iet.commons.ldap.dao.LdapMailListingDao;
import edu.ucdavis.iet.commons.ldap.dao.LdapSearchType;
import edu.ucdavis.iet.commons.ldap.domain.LdapMailListing;
import edu.ucdavis.iet.commons.ldap.domain.util.LdapMailListingConstants;
import edu.ucdavis.iet.commons.ldap.service.LdapMailListingService;

public class LdapMailListingServiceImpl implements LdapMailListingService
{
    /**
     * Injected LDAP Mail Listing Data Access Object
     */
    private LdapMailListingDao ldapMailListingDao;
    public void setLdapMailListingDao(LdapMailListingDao ldapMailListingDao)
    {
        this.ldapMailListingDao = ldapMailListingDao;
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.service.LdapMailListingService#findMailListings(String)
     */
    @Override
    public List<LdapMailListing> findMailListings(String uuid)
    {
        if (StringUtils.isBlank(uuid))
        {
            throw new IllegalArgumentException("Universal User ID is blank or null.");
        }
        return ldapMailListingDao.findMailListings(uuid);
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.service.LdapMailListingService#findPrimaryMailListing(String)
     */
    @Override
    public LdapMailListing findPrimaryMailListing(String uuid)
    {
        if (StringUtils.isBlank(uuid))
        {
            throw new IllegalArgumentException("Universal User ID is blank or null.");
        }
        try
        {
            return ldapMailListingDao.findPrimaryMailListing(uuid);
        }
        catch (EmptyResultDataAccessException erdae)
        {
            return null;
        }
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.service.LdapMailListingService#findByCriteria(Map)
     */
    @Override
    public List<LdapMailListing> findByCriteria(Map<String, String> searchCriteria)
    {
        if (searchCriteria == null || searchCriteria.isEmpty())
        {
            throw new IllegalArgumentException("Search criteria are empty or null.");
        }
        return ldapMailListingDao.findByCriteria(searchCriteria);
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.service.LdapMailListingService#findByCriteria(Map, LdapSearchType)
     */
    @Override
    public List<LdapMailListing> findByCriteria(Map<String, String> searchCriteria, LdapSearchType searchType)
    {
        if (searchCriteria == null || searchCriteria.isEmpty())
        {
            throw new IllegalArgumentException("Search criteria are empty or null.");
        }
        return ldapMailListingDao.findByCriteria(searchCriteria, searchType);
    }
    
    /**
     * @see edu.ucdavis.iet.commons.ldap.service.LdapMailListingService#findByEmailAddress(String)
     */    
    @Override
    public List<LdapMailListing> findByEmailAddress(String mail)
    {
        if (StringUtils.isBlank(mail))
        {
            throw new IllegalArgumentException("E-mail Address is blank or null.");
        }
        Map<String, String> searchCriteria = new HashMap<String, String>(1);
        searchCriteria.put(LdapMailListingConstants.EMAIL_ADDRESS, mail);
        return ldapMailListingDao.findByCriteria(searchCriteria);
    }
}
