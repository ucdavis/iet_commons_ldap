package edu.ucdavis.iet.commons.ldap.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.AbstractContextMapper;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;

import edu.ucdavis.iet.commons.ldap.dao.LdapAccountDao;
import edu.ucdavis.iet.commons.ldap.domain.LdapAccount;
import edu.ucdavis.iet.commons.ldap.domain.impl.LdapAccountImpl;
import edu.ucdavis.iet.commons.ldap.domain.util.LdapAccountConstants;
import edu.ucdavis.iet.commons.ldap.domain.util.LdapSchemaConstants;

public class LdapAccountDaoSpringImpl implements LdapAccountDao
{
    /*
     * Injected LDAP Template
     */
    private LdapTemplate ldapTemplate;
    public void setLdapTemplate(LdapTemplate ldapTemplate)
    {
        this.ldapTemplate = ldapTemplate;
    }

    /*
     * Map fetched LDAP Account attributes to LDAP Account object
     */
    private static class LdapAccountContextMapper extends AbstractContextMapper
    {
        protected Object doMapFromContext(DirContextOperations context)
        {
            Map<String, String> ldapAccountContextMap = new HashMap<String, String>(2);
            ldapAccountContextMap.put(LdapAccountConstants.USER_ID_NUMBER, context.getStringAttribute(LdapAccountConstants.USER_ID_NUMBER));
            ldapAccountContextMap.put(LdapAccountConstants.USER_ID, context.getStringAttribute(LdapAccountConstants.USER_ID));
            LdapAccount ldapAccount = new LdapAccountImpl(ldapAccountContextMap);
            return ldapAccount;
        }
    }

    /*
     * Return a new LDAP Account Context Mapper
     */
    private ContextMapper getContextMapper()
    {
        return new LdapAccountContextMapper();
    }
    
    /**
     * @see edu.ucdavis.iet.commons.ldap.dao.LdapAccountDao#findAccountsByUserID(java.lang.String)
     */
    @Override
    public List<LdapAccount> findAccountsByUserID(String uid)
    {
        return findBySingleAttribute(LdapAccountConstants.USER_ID, uid);
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.dao.LdapAccountDao#findAccountsByUserIDNumber(java.lang.String)
     */
    @Override
    public List<LdapAccount> findAccountsByUserIDNumber(String uidNumber)
    {
        return findBySingleAttribute(LdapAccountConstants.USER_ID_NUMBER, uidNumber);
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.dao.LdapAccountDao#findByUserId(java.lang.String, java.lang.String)
     */
    @Override
    public LdapAccount findByUserId(String uid, String serviceName)
    {
        return findUniqueDirectoryObjectByServiceName(LdapAccountConstants.USER_ID, uid, serviceName);
    }

    /**
     * @see edu.ucdavis.iet.commons.ldap.dao.LdapAccountDao#findByUserIdNumber(java.lang.String, java.lang.String)
     */
    @Override
    public LdapAccount findByUserIdNumber(String uidNumber, String serviceName)
    {
        return findUniqueDirectoryObjectByServiceName(LdapAccountConstants.USER_ID_NUMBER, uidNumber, serviceName);
    }
    
    /*
     * Find by Service Name, where Service Name is part of the Distinguished Name
     */
    private LdapAccount findUniqueDirectoryObjectByServiceName(String objectAttribute, String objectAttributeValue, String serviceName)
    {
        if (StringUtils.isBlank(objectAttribute))
        {
            throw new IllegalArgumentException("Parameter " + objectAttribute + " is blank or null.");
        }
        if (StringUtils.isBlank(objectAttributeValue))
        {
            throw new IllegalArgumentException("Search value is blank or null.");
        }
        if (StringUtils.isBlank(serviceName))
        {
            throw new IllegalArgumentException("Parameter 'serviceName' is blank or null.");
        }
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter(objectAttribute, objectAttributeValue));
        DistinguishedName dn = new DistinguishedName();
        dn.add(LdapSchemaConstants.LDAP_OU, LdapSchemaConstants.ACCOUNTS_SCHEMA_NAME);
        dn.add(LdapSchemaConstants.LDAP_OU, serviceName);
        return (LdapAccount) ldapTemplate.searchForObject(dn, filter.encode(), getContextMapper());        
    }
    
    /*
     * Find by a single attribute using a general search
     */
    @SuppressWarnings("unchecked")
    private List<LdapAccount> findBySingleAttribute(String objectAttribute, String objectAttributeValue)
    {
        if (StringUtils.isBlank(objectAttribute))
        {
            throw new IllegalArgumentException("Parameter " + objectAttribute + " is blank or null.");
        }
        if (StringUtils.isBlank(objectAttributeValue))
        {
            throw new IllegalArgumentException("Search value is blank or null.");
        }
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter(LdapSchemaConstants.OBJECT_CLASS, "posixAccount"));
        filter.and(new EqualsFilter(objectAttribute, objectAttributeValue));
        DistinguishedName dn = new DistinguishedName();
        dn.add(LdapSchemaConstants.LDAP_OU, LdapSchemaConstants.ACCOUNTS_SCHEMA_NAME);
        return (List<LdapAccount>) ldapTemplate.search(dn, filter.encode(), getContextMapper());
    }
}

