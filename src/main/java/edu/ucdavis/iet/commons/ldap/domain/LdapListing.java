package edu.ucdavis.iet.commons.ldap.domain;

/**
 * This class represents a single entry in the ou=Listings schema in the UC Davis LDAP Directory.
 */

public interface LdapListing {

	/**
	 *  Universal User ID - also known as Entity ID or Mothra ID
	 */
	public String getUcdPersonUUID();
	
	/**
	 *  Listing Order
	 */
	public String getUcdListingOrder();
	
	/**
	 *  Display Order
	 */
	public String getUcdDisplayOrder();
	
	/**
	 *  Grouping Order
	 */
	public String getUcdGroupingOrder();
	
	/**
	 *  Publish Flag
	 */
	public String getUcdPublish();
	
}
