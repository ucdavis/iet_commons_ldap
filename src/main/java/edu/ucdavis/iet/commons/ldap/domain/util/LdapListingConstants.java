package edu.ucdavis.iet.commons.ldap.domain.util;

/**
 * This class maps human readable attributes to LDAP Mail Listing object
 * attributes. It is helpful for mapping attributes between LDAP Listing
 * records and external system records.
 * 
 * @author eldavid
 * 
 */
public class LdapListingConstants
{

    /**
     * Universal User ID - also known as Entity ID or Mothra ID
     */
    public static final String UNIVERSAL_USER_ID = "ucdPersonUUID";

    /**
     * Listing Order
     */
    public static final String LISTING_ORDER = "ucdListingOrder";

    /**
     * Grouping Order
     */
    public static final String GROUPING_ORDER = "ucdGroupingOrder";

    /**
     * Display Order
     */
    public static final String DISPLAY_ORDER = "ucdDisplayOrder";

    /**
     * Publish Flag - N: Private, R: Restricted, W:World
     */
    public static final String PUBLISH_FLAG = "ucdPublish";
    
    /**
     * Uniform Resource Identifier - Object Identifier
     */
    public static final String URI_OID = "ucdURIOID";

}
