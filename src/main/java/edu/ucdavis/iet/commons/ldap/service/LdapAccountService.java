package edu.ucdavis.iet.commons.ldap.service;

import java.util.List;

import edu.ucdavis.iet.commons.ldap.domain.LdapAccount;

/**
 * IET LDAP Account Service API for fetching records of Service Accounts from
 * LDAP.
 * 
 * @author eldavid
 * 
 */
public interface LdapAccountService
{
    /**
     * Find all Accounts by User ID Number
     * 
     * @param uidNumber
     *            User ID Number
     * 
     * @return A list of LDAP Accounts
     */
    public List<LdapAccount> findAccountsByUserIDNumber(String uidNumber);

    /**
     * Find all Accounts by User ID
     * 
     * @param uid
     *            User ID
     * @return A list of LDAP Accounts
     */
    public List<LdapAccount> findAccountsByUserID(String uid);

    /**
     * Find Account by User ID Number and Service Name
     * 
     * @param uidNumber
     *            User ID Number
     * @param serviceName
     *            Service Name
     * @return A single LDAP Account
     */
    public LdapAccount findByUserIdNumber(String uidNumber, String serviceName);

    /**
     * Find Account by User ID and Service Name
     * 
     * @param uid
     *            User ID
     * @param serviceName
     *            Service Name
     * @return A single LDAP Account
     */
    public LdapAccount findByUserId(String uid, String serviceName);
}
