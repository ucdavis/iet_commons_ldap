package edu.ucdavis.iet.commons.ldap.domain.util;

/**
 * This class maps human readable attributes to LDAP Person object attributes.
 * It is helpful for mapping attributes between LDAP Person records and external
 * system records.
 *
 * @author eldavid
 *
 */
public class LdapPersonConstants
{
    /**
     * Country
     */
	public static final String COUNTRY = "c";

	/**
     * Universal User ID - also known as Entity ID or Mothra ID
     */
    public static final String UNIVERSAL_USER_ID = "ucdPersonUUID";

    /**
     * User ID - also known as User Account or Principal ID
     */
    public static final String USER_ID = "uid";

    /**
     * Formatted "White Pages" Name
     */
    public static final String DISPLAY_NAME = "displayName";

    /**
     * First Name
     */
    public static final String FIRST_NAME = "givenName";

    /**
     * Last Name
     */
    public static final String LAST_NAME = "sn";

    /**
     * Full Name - also known as Common Name
     */
    public static final String FULL_NAME = "cn";

    /**
     * Nick Name
     */
    public static final String NICK_NAME = "eduPersonNickName";

    /**
     * E-mail Address
     */
    public static final String EMAIL_ADDRESS = "mail";

    /**
     * Phone Number
     */
    public static final String PHONE_NUMBER = "telephoneNumber";

    /**
     * Street
     */
    public static final String STREET = "street";

    /**
     * City
     */
    public static final String CITY_NAME = "l";

    /**
     * State
     */
    public static final String STATE_CODE = "st";

    /**
     * Postal Code - also known as ZIP Code
     */
    public static final String POSTAL_CODE = "postalCode";

    /**
     * Composition of Street, City, State, and Postal Code
     */
    public static final String POSTAL_ADDRESS = "postalAddress";

    /**
     * Employee ID - also known as Employee Number
     */
    public static final String EMPLOYEE_ID = "employeeNumber";

    /**
     * Title - also known as Job Title or Position Title
     */
    public static final String EMPLOYEE_TITLE = "title";

    /**
     * Affiliation(s) with UCD
     */
    public static final String AFFILIATION_TYPE_CODE = "eduPersonAffiliation";

    /**
     * Affiliation(s) with UCD - An augmented form of eduPersonAffiliation
     */
    public static final String UCD_AFFILIATION_TYPE_CODE = "ucdPersonAffiliation";

    /**
     * Home Department Number - also known as Primary Department Number
     */
    public static final String PRIMARY_DEPARTMENT_CODE = "departmentNumber";

    /**
     * Home Department Name
     */
    public static final String PRIMARY_DEPARTMENT_NAME = "ou";
    
    /**
     * Pager
     */
    public static final String PAGER="pager";

    /**
     * Contains the list of values for the "publish" flags that follow.
     *   The values, if they exist, have the following form:
     *      ucdPublishSomeItem:N or  (Do not publish)
     *      ucdPublishSomeItem:R or  (Publish to ucdavis domain only - intranet)
     *      ucdPublishSomeItem:W     (Publish to world wide web - internet)
     */
    public static final String UCD_PUBLISH_FLAGS = "ucdPublishItemFlag";

    /**
     * UCD Publish Flag for all attributes
     */
    public static final String UCD_PUBLISH_FLAG_ALL_ATTR = "ucdPublishPerson";

    /**
     * UCD Publish Flag for Title attribute
     */
    public static final String UCD_PUBLISH_TITLE = "ucdPublishTitle";

    /**
     * UCD Publish Flag for Department attribute
     */
    public static final String UCD_PUBLISH_DEPARTMENT = "ucdPublishDepartment";

    /**
     * UCD Publish Flag for Address attributes (including postalAddress, street, locality, state, postalCode,
     *     country, roomNumber, and physical Delivery Office Name
     */
    public static final String UCD_PUBLISH_ADDRESS = "ucdPublishAddress";

    /**
     * UCD Publish Flag for Phone attribute
     */
    public static final String UCD_PUBLISH_PHONE = "ucdPublishPhone";

    /**
     * UCD Publish Flag for Mobile Phone attribute
     */
    public static final String UCD_PUBLISH_MOBILE = "ucdPublishMobile";

    /**
     * UCD Publish Flag for Fax attribute
     */
    public static final String UCD_PUBLISH_FAX = "ucdPublishFax";

    /**
     * UCD Publish Flag for Mail attribute
     */
    public static final String UCD_PUBLISH_MAIL = "ucdPublishMail";

    /**
     * UCD Publish Flag for Labeled Uri attribute
     */
    public static final String UCD_PUBLISH_URI = "ucdPublishLabeledUri";
}
