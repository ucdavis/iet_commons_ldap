package edu.ucdavis.iet.commons.ldap.service;

import java.util.List;
import java.util.Map;

import edu.ucdavis.iet.commons.ldap.dao.LdapSearchType;
import edu.ucdavis.iet.commons.ldap.domain.LdapPerson;

/**
 * IET LDAP Person Service API for fetching records of persons from LDAP.
 * 
 * @author eldavid
 * 
 */
public interface LdapPersonService
{

    /**
     * Find an LDAP Person by a Universal User ID
     * 
     * @param uuid
     *            Universal User ID - Uniquely identifies each record in LDAP
     * @return An LDAP Person
     */
    public LdapPerson findByUniversalUserId(String uuid);

    /**
     * Find one or more LDAP Persons by one or more Universal User IDs
     * 
     * @param uuids
     *            List of Universal User IDs
     * @return A list of LDAP Persons
     */
    public List<LdapPerson> findByUniversalUserId(List<String> uuids);

    /**
     * Find an LDAP person by User ID
     * 
     * @param uid
     *            User ID - Kerberos ID
     * @return An LDAP person
     */
    public LdapPerson findByUserId(String uid);

    /**
     * Find one or more LDAP Persons by Home Department Number
     * 
     * @param departmentNumber
     *            Home Department Number
     * @return A list of LDAP Persons
     */
    public List<LdapPerson> findByDepartmentId(String departmentNumber);

    /**
     * Search for one or more LDAP Persons by one or more criteria
     * 
     * @param searchCriteria
     *            Attributes to search by, in the form of key-value pairs
     * @return A list of LDAP Persons
     */
    public List<LdapPerson> findByCriteria(Map<String, String> searchCriteria);

    /**
     * Search for one or more LDAP Persons by one or more criteria, specifying
     * whether the client wants an exact match or wildcard match. If exactMatch
     * is true, the method will treat the criteria as exact criteria. If
     * exactMatch is false, the method will treat the criteria as wildcard
     * criteria.
     * 
     * @param searchCriteria
     *            Attributes to search by, in the form of key-value pairs
     * @return A list of LDAP Persons
     */
    public List<LdapPerson> findByCriteria(Map<String, String> searchCriteria, boolean exactMatch);
    
	/**
	 * Search for one or more LDAP Persons by one or more criteria, specifying what type
	 * of search should be performed (e.g. exact, wildcard, like, etc). 
	 * See LdapSearchType for details.
	 * 
	 * @param searchCriteria	Attributes to search by, in the form of key-value pairs
	 * @param searchType		Determine how the searchCriteria should be filtered by LDAP search
	 * @return					A list of LDAP Persons 
	 */
	public List<LdapPerson> findByCriteria(Map<String,String> searchCriteria, LdapSearchType searchType);	    
}
