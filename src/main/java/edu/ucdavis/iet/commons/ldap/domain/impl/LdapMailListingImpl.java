package edu.ucdavis.iet.commons.ldap.domain.impl;

import java.util.Map;

import edu.ucdavis.iet.commons.ldap.domain.LdapMailListing;
import edu.ucdavis.iet.commons.ldap.domain.util.LdapMailListingConstants;

/**
 * Base implementation of the LdapMailListing domain object.
 */
public class LdapMailListingImpl implements LdapMailListing {

	private String ucdUriOid;
	private String ucdPersonUUID;
	private String mail;
	private String ucdDisplayOrder;
	private String ucdGroupingOrder;
	private String ucdListingOrder;
	private String ucdPublish;
	
	/**
	 * Compose attributes from a Map 
	 * @param ldapMailListingAttributeMap
	 */
	public LdapMailListingImpl(Map<String,String> ldapMailListingAttributeMap) {
		this.ucdUriOid = ldapMailListingAttributeMap.get(LdapMailListingConstants.URI_OID);
		this.mail = ldapMailListingAttributeMap.get(LdapMailListingConstants.EMAIL_ADDRESS);
		this.ucdDisplayOrder = ldapMailListingAttributeMap.get(LdapMailListingConstants.DISPLAY_ORDER);
		this.ucdGroupingOrder = ldapMailListingAttributeMap.get(LdapMailListingConstants.GROUPING_ORDER);
		this.ucdListingOrder = ldapMailListingAttributeMap.get(LdapMailListingConstants.LISTING_ORDER);
		this.ucdPersonUUID = ldapMailListingAttributeMap.get(LdapMailListingConstants.UNIVERSAL_USER_ID);
		this.ucdPublish = ldapMailListingAttributeMap.get(LdapMailListingConstants.PUBLISH_FLAG);		
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapMailListing#getMail()
	 */
	public String getMail() {
		return mail;
	}
	
	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapMailListing#getUcdUriOid()
	 */
	public String getUcdUriOid() {
		return ucdUriOid;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapMailListing#getUcdDisplayOrder()
	 */
	public String getUcdDisplayOrder() {
		return ucdDisplayOrder;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapMailListing#getUcdGroupingOrder()
	 */
	public String getUcdGroupingOrder() {
		return ucdGroupingOrder;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapMailListing#getUcdListingOrder()
	 */
	public String getUcdListingOrder() {
		return ucdListingOrder;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapMailListing#getUcdPersonUUID()
	 */
	public String getUcdPersonUUID() {
		return ucdPersonUUID;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapMailListing#getUcdPublish()
	 */
	public String getUcdPublish() {
		return ucdPublish;
	}

	/**
	 * Constructs a String with all attributes in name = value format.
	 *
	 * @return a String representation of this object.
	 */
	// IETC-29: implement toString()
	public String toString() {
	    final String TAB = "\n";
	    
	    String retValue = "LdapMailListingDaoSpringImpl ( "
	        + "ucdUriOid = " + this.ucdUriOid + TAB
	        + "ucdPersonUUID = " + this.ucdPersonUUID + TAB
	        + "mail = " + this.mail + TAB
	        + "ucdDisplayOrder = " + this.ucdDisplayOrder + TAB
	        + "ucdGroupingOrder = " + this.ucdGroupingOrder + TAB
	        + "ucdListingOrder = " + this.ucdListingOrder + TAB
	        + "ucdPublish = " + this.ucdPublish + TAB
	        + " )";
	    return retValue;
	}
}
