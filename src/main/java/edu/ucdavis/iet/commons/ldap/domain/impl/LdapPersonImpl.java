package edu.ucdavis.iet.commons.ldap.domain.impl;

import java.util.ArrayList;
import java.util.List;

import edu.ucdavis.iet.commons.ldap.domain.LdapPerson;

/**
 * Base implementation of the LdapPerson domain object.
 */

public class LdapPersonImpl implements LdapPerson {

	private String c;
	private String dn;
	private String ucdPersonUUID;
	private String[] uid;
	private String displayName;
	private String[] mail;
	private String[] telephoneNumber;
	private String[] street;
	private String[] l;
	private String[] st;
	private String[] postalCode;
	private String[] postalAddress;
	private String eduPersonNickName;
	private String givenName;
	private String[] sn;
	private String[] cn;
	private String[] eduPersonAffiliation;
	private String[] ucdPersonAffiliation;
	private String[] departmentNumber;
	private String[] ou;
	private String pager;
	private String[] title;
	private String employeeNumber;
	private String[] ucdPublishItemFlag;
	private String ucdPublishPerson;
	private String ucdPublishTitle;
	private String ucdPublishDepartment;
	private String ucdPublishAddress;
	private String ucdPublishPhone;
	private String ucdPublishMobile;
	private String ucdPublishFax;
	private String ucdPublishMail;
	private String ucdPublishLabeledUri;

	/**
	 * Default constructor
	 */
	public LdapPersonImpl() {
	}

	/**
	 * @param dn the dn to set
	 */
	public void setDn(String dn) {
		this.dn = dn;
	}

	/**
	 * @return the dn
	 */
	public String getDn() {
		return dn;
	}

	/**
	 * edu.ucdavis.iet.ws.domain.directory.DirectoryResult#getC()
	 */
	public String getC() {
		return c;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getCn()
	 */
	public List<String> getCn() {
		return convertToList(cn);
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getDepartmentNumber()
	 */
	public List<String> getDepartmentNumber() {
		return convertToList(departmentNumber);
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getDisplayName()
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getEduPersonAffiliation()
	 */
	public List<String> getEduPersonAffiliation() {
		return convertToList(eduPersonAffiliation);
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getUcdPersonAffiliation()
	 */
	public List<String> getUcdPersonAffiliation() {
		return convertToList(ucdPersonAffiliation);
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getEmployeeNumber()
	 */
	public String getEmployeeNumber() {
		return employeeNumber;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getGivenName()
	 */
	public String getGivenName() {
		return givenName;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getL()
	 */
	public List<String> getL() {
		return convertToList(l);
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getMail()
	 */
	public List<String> getMail() {
		return convertToList(mail);
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getOu()
	 */
	public List<String> getOu() {
		return convertToList(ou);
	}
	
	/**
	 * edu.ucdavis.iet.ws.domain.directory.DirectoryResult#getPager()
	 */
	public String getPager() {
		return pager;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getPostalAddress()
	 */
	public List<String> getPostalAddress() {
		return convertToList(postalAddress);
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getPostalCode()
	 */
	public List<String> getPostalCode() {
		return convertToList(postalCode);
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getSn()
	 */
	public List<String> getSn() {
		return convertToList(sn);
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getSt()
	 */
	public List<String> getSt() {
		return convertToList(st);
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getStreet()
	 */
	public List<String> getStreet() {
		return convertToList(street);
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getTelephoneNumber()
	 */
	public List<String> getTelephoneNumber() {
		return convertToList(telephoneNumber);
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getTitle()
	 */
	public List<String> getTitle() {
		return convertToList(title);
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getUcdPersonUUID()
	 */
	public String getUcdPersonUUID() {
		return ucdPersonUUID;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getUid()
	 */
	public List<String> getUid() {
		return convertToList(uid);
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getEduPersonNickName()
	 */
	public String getEduPersonNickName() {
		return eduPersonNickName;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getUcdPublishItemFlag()
	 */
	public List<String> getUcdPublishItemFlag() {
		return convertToList(ucdPublishItemFlag);
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getUcdPublishPerson()
	 */
	public String getUcdPublishPerson() {
		return ucdPublishPerson;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getUcdPublishTitle()
	 */
	public String getUcdPublishTitle() {
		return ucdPublishTitle;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getUcdPublishDepartment()
	 */
	public String getUcdPublishDepartment() {
		return ucdPublishDepartment;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getUcdPublishAddress()
	 */
	public String getUcdPublishAddress() {
		return ucdPublishAddress;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getUcdPublishPhone()
	 */
	public String getUcdPublishPhone() {
		return ucdPublishPhone;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getUcdPublishMobile()
	 */
	public String getUcdPublishMobile() {
		return ucdPublishMobile;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getUcdPublishFax()
	 */
	public String getUcdPublishFax() {
		return ucdPublishFax;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getUcdPublishMail()
	 */
	public String getUcdPublishMail() {
		return ucdPublishMail;
	}

	/**
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getUcdPublishLabeledUri()
	 */
	public String getUcdPublishLabeledUri() {
		return ucdPublishLabeledUri;
	}

	/**
	 * Convert String[] to List<String>
	 * 
	 * @param attributeArray
	 *            Array of Strings
	 * @return List of Strings
	 */
	@SuppressWarnings("unchecked")
	private List<String> convertToList(String[] attributeArray) {
		if (attributeArray == null) {
			return null;
		}
		List attributeList = new ArrayList();
		for (String attributeValue : attributeArray) {
			attributeList.add(attributeValue);
		}
		return attributeList;
	}

	/**
	 * 
	 * edu.ucdavis.iet.ws.domain.directory.DirectoryReault#getC()
	 */
	
	public void setC(String c) {
		this.c = c;
	}
	
	/**
	 * Set the Universal User ID
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getUcdPersonUUID()
	 */
	public final void setUcdPersonUUID(String ucdPersonUUID) {
		this.ucdPersonUUID = ucdPersonUUID;
	}

	/**
	 * Set the User ID
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getUid()
	 */
	public final void setUid(String[] uid) {
		this.uid = uid;
	}

	/**
	 * Set the Display Name
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getDisplayName()
	 */
	public final void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * Set the Last Name
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getSn()
	 */
	public final void setSn(String[] sn) {
		this.sn = sn;
	}

	/**
	 * Set the First Name
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getGivenName()
	 */
	public final void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	/**
	 * Set the Full Name
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getCn()
	 */
	public final void setCn(String[] cn) {
		this.cn = cn;
	}

	/**
	 * Set the E-mail Address
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getMail()
	 */
	public final void setMail(String[] mail) {
		this.mail = mail;
	}

	/**
	 * Set the Phone Number
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getTelephoneNumber()
	 */
	public final void setTelephoneNumber(String[] telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	/**
	 * Set the Street
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getStreet()
	 */
	public final void setStreet(String[] street) {
		this.street = street;
	}

	/**
	 * Set the City or Locale
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getL()
	 */
	public final void setL(String[] l) {
		this.l = l;
	}

	/**
	 * Set the State Code
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getSt()
	 */
	public final void setSt(String[] st) {
		this.st = st;
	}

	/**
	 * Set the Postal Code
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getPostalCode()
	 */
	public final void setPostalCode(String[] postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * Set the Postal Address
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getPostalAddress()
	 */
	public final void setPostalAddress(String[] postalAddress) {
		this.postalAddress = postalAddress;
	}

	/**
	 * Set the EDU Affiliation
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getEduPersonAffiliation()
	 */
	public final void setEduPersonAffiliation(String[] eduPersonAffiliation) {
		this.eduPersonAffiliation = eduPersonAffiliation;
	}

	/**
	 * Set the UCD Affiliation
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getEduPersonAffiliation()
	 */
	public final void setUcdPersonAffiliation(String[] ucdPersonAffiliation) {
		this.ucdPersonAffiliation = ucdPersonAffiliation;
	}

	/**
	 * Set the Department Number
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getDepartmentNumber()
	 */
	public final void setDepartmentNumber(String[] departmentNumber) {
		this.departmentNumber = departmentNumber;
	}

	/**
	 * Set the Home Department Name
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getOu()
	 */
	public final void setOu(String[] ou) {
		this.ou = ou;
	}

	/**
	 * Set Pager number
	 * edu.ucdavis.iet.ws.domain.Directory.DirectoryResult#getPager()
	 */
	public void setPager(String pager) {
		this.pager = pager;
	}
	
	/**
	 * Set the Employee Title
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getTitle()
	 */
	public final void setTitle(String[] title) {
		this.title = title;
	}

	/**
	 * Set the Employee Number
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getEmployeeNumber()
	 */
	public final void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	/**
	 * Set Nickname
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getEduPersonNickName()
	 */
	public final void setEduPersonNickName(String eduPersonNickName) {
		this.eduPersonNickName = eduPersonNickName;
	}

	/**
	 * Set ucdPublishPerson
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getUcdPublishPerson()
	 */

	public final void setUcdPublishPerson(String ucdPublishPerson) {
		this.ucdPublishPerson = ucdPublishPerson;
	}

	/**
	 * Set ucdPublishTitle
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getucdPublishTitle()
	 */
	public final void setUcdPublishTitle(String ucdPublishTitle) {
		this.ucdPublishTitle = ucdPublishTitle;
	}

	/**
	 * Set ucdPublishDepartment
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getucdPublishDepartment()
	 */
	public final void setUcdPublishDepartment(String ucdPublishDepartment) {
		this.ucdPublishDepartment = ucdPublishDepartment;
	}

	/**
	 * Set ucdPublishAddress
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getucdPublishAddress()
	 */
	public final void setUcdPublishAddress(String ucdPublishAddress) {
		this.ucdPublishAddress = ucdPublishAddress;
	}

	/**
	 * Set ucdPublishPhone
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getucdPublishPhone()
	 */
	public final void setUcdPublishPhone(String ucdPublishPhone) {
		this.ucdPublishPhone = ucdPublishPhone;
	}

	/**
	 * Set ucdPublishMobile
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getucdPublishMobile()
	 */
	public final void setUcdPublishMobile(String ucdPublishMobile) {
		this.ucdPublishMobile = ucdPublishMobile;
	}

	/**
	 * Set ucdPublishFax
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getucdPublishFax()
	 */
	public final void setUcdPublishFax(String ucdPublishFax) {
		this.ucdPublishFax = ucdPublishFax;
	}

	/**
	 * Set ucdPublishMail
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getucdPublishMail()
	 */
	public final void setUcdPublishMail(String ucdPublishMail) {
		this.ucdPublishMail = ucdPublishMail;
	}

	/**
	 * Set ucdPublishLabeledUri
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getucdPublishLabeledUri()
	 */
	public final void setUcdPublishLabeledUri(String ucdPublishLabeledUri) {
		this.ucdPublishLabeledUri = ucdPublishLabeledUri;
	}

	/**
	 * Set ucdPublishItemFlag
	 * 
	 * @see edu.ucdavis.iet.commons.ldap.domain.LdapPerson#getucdPublishLabeledUri()
	 */
	public final void setUcdPublishItemFlag(String[] ucdPublishItemFlag) {
		this.ucdPublishItemFlag = ucdPublishItemFlag;
	}

	/**
	 * Constructs a String with all attributes in name = value format.
	 * 
	 * @return a String representation of this object.
	 */
	// IETC-29: implement toString()
	public String toString() 
	{
		final String TAB = "\n";

		String retValue = "LdapMailListingDaoSpringImpl ( " 
			+ "dn = " + this.dn + TAB 
			+ "ucdPersonUUID = " + this.ucdPersonUUID + TAB 
			+ "uid = " + listToString(this.uid) + TAB
			+ "displayName = " + this.displayName + TAB
			+ "telephoneNumber = " + listToString(this.telephoneNumber) + TAB
			+ "street = " + this.street + TAB
			+ "l = " + this.l + TAB
			+ "st = " + this.st + TAB
			+ "postalCode = " + this.postalCode + TAB
			+ "postalAddress = " + this.postalAddress + TAB
			+ "eduPersonNickName = " + this.eduPersonNickName + TAB
			+ "givenName = " + this.givenName + TAB
			+ "sn = " + listToString(this.sn) + TAB
			+ "cn = " + listToString(this.cn )+ TAB
			+ "eduPersonAffiliation = " + listToString(this.eduPersonAffiliation) + TAB
			+ "ucdPersonAffiliation = " + listToString(this.ucdPersonAffiliation) + TAB
			+ "c = " + this.c + TAB 
			+ "departmentNumber = " + listToString(this.departmentNumber) + TAB
			+ "ou = " + listToString(this.ou) + TAB
			+ "pager = " + this.pager + TAB 
			+ "employeeNumber = " + this.employeeNumber + TAB
			+ "ucdPublishItemFlag = " + listToString(this. ucdPublishItemFlag) + TAB
			+ "ucdPublishPerson = " + this.ucdPublishPerson + TAB
			+ "ucdPublishTitle = " + this.ucdPublishTitle + TAB
			+ "ucdPublishDepartment = " + this.ucdPublishDepartment + TAB
			+ "ucdPublishAddress = " + this.ucdPublishAddress + TAB
			+ "ucdPublishPhone = " + this.ucdPublishPhone + TAB
			+ "ucdPublishMobile = " + this.ucdPublishMobile + TAB
			+ "ucdPublishFax = " + this.ucdPublishFax + TAB
			+ "ucdPublishMail = " + this.ucdPublishMail + TAB
			+ "ucdPublishLabeledUri = " + this.ucdPublishLabeledUri + TAB
			+ " )";
		return retValue;
	}
	
	/*
	 * Convert String[] to a String of comma-delimited values
	 */
	// IETC-29: implement toString()
	private String listToString(String[] attributeList) 
	{
		String delimitedAttributeList = "";
		// IETC-41 - Protect ourselves from nulls
		if (attributeList == null)
		{
			return delimitedAttributeList;
		}
		else
		{
			for(String attribute : attributeList)
			{
				delimitedAttributeList = delimitedAttributeList + attribute + ",";
			}
			return delimitedAttributeList.substring(0, delimitedAttributeList.length() - 1);
		}
	}


	
}
