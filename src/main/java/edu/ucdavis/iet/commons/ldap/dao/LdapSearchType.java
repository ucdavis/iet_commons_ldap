package edu.ucdavis.iet.commons.ldap.dao;

/**
 * Defines the type of LDAP filters to be used. 
 * 
 * Leverages Spring LDAP filter API as defined by http://static.springsource.org/spring-ldap/site/apidocs/org/springframework/ldap/filter/package-summary.html
 * 
 * @author clbray
 *
 */
public enum LdapSearchType {

	/**
	 * Utilize 'Equals' filter as defined by http://static.springsource.org/spring-ldap/site/apidocs/org/springframework/ldap/filter/EqualsFilter.html
	 */
	Equals,
	
	/**
	 * Utilize 'Like' filter as defined by http://static.springsource.org/spring-ldap/site/apidocs/org/springframework/ldap/filter/LikeFilter.html
	 */
	Like,
	
	/**
	 * Utilize 'WhitespaceWildecards' filter as defined by http://static.springsource.org/spring-ldap/site/apidocs/org/springframework/ldap/filter/WhitespaceWildcardsFilter.html
	 */
	WhitespaceWildcards
	
}
