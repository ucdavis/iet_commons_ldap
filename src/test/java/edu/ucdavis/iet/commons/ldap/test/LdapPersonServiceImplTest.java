package edu.ucdavis.iet.commons.ldap.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ldap.LimitExceededException;
import org.springframework.ldap.NameNotFoundException;

import junit.framework.Assert;
import junit.framework.TestCase;

import edu.ucdavis.iet.commons.ldap.dao.LdapSearchType;
import edu.ucdavis.iet.commons.ldap.domain.LdapMailListing;
import edu.ucdavis.iet.commons.ldap.domain.LdapPerson;
import edu.ucdavis.iet.commons.ldap.domain.impl.LdapPersonImpl;
import edu.ucdavis.iet.commons.ldap.domain.util.LdapPersonConstants;
import edu.ucdavis.iet.commons.ldap.service.LdapMailListingService;
import edu.ucdavis.iet.commons.ldap.service.LdapPersonService;

public class LdapPersonServiceImplTest extends TestCase
{
    private String[] configLocations = {"classpath*:edu/ucdavis/iet/commons/ldap/test/config/ldap-common.xml"
                                       ,"classpath*:edu/ucdavis/iet/commons/ldap/test/config/ldap-data.xml"
                                       ,"classpath*:edu/ucdavis/iet/commons/ldap/test/config/ldap-anonymous-data.xml"
                                       ,"classpath*:edu/ucdavis/iet/commons/ldap/test/config/ldap-service.xml"
                                       ,"classpath*:edu/ucdavis/iet/commons/ldap/config/iet-commons-ldap.xml"};
    private ApplicationContext context = new ClassPathXmlApplicationContext(configLocations);
    private LdapPersonService ldapPersonService = (LdapPersonService) context.getBean("ldapPersonService");
    private LdapPersonService anonymousLdapPersonService = (LdapPersonService) context.getBean("anonymousLdapPersonService");
    private LdapMailListingService ldapMailListingService = (LdapMailListingService) context.getBean("ldapMailListingService");
    private List<String> universalUserIdList = new ArrayList<String>();
    private LdapMailListing chancellorsMailListing = ldapMailListingService.findByEmailAddress("chancellor@ucdavis.edu").get(0);
    private LdapPersonImpl unpublishedPerson = new LdapPersonImpl();

    protected void setUp() throws Exception
    {

        // Initialize a list of LDAP Persons
        universalUserIdList.add("00002058");
        universalUserIdList.add("00002059");
        universalUserIdList.add("00002060");
        universalUserIdList.add("00002051");

        // Initialize an unpublished Person
        unpublishedPerson.setUid(getSingleNodeStringArray("moslis"));
    }

    /*
     * Find an LDAP Person by a valid Universal User ID
     */
    public void testFindByValidUniversalUserId()
    {
        LdapPerson actualResult = ldapPersonService.findByUniversalUserId(chancellorsMailListing.getUcdPersonUUID());
        Assert.assertNotNull(actualResult);
        Assert.assertEquals(chancellorsMailListing.getUcdPersonUUID(), actualResult.getUcdPersonUUID());
    }

    /*
     * Find an LDAP Person by an invalid Universal User ID
     */
    public void testFindByInvalidUniversalUserId()
    {
        try
        {
            LdapPerson actualResult = ldapPersonService.findByUniversalUserId("00002060");
        }
        catch (NameNotFoundException nnfe)
        {
            Assert.assertNotNull(nnfe);
        }
    }

    /*
     * Find an LDAP Person by an invalid User ID
     */
    public void testFindByInvalidUserId()
    {
        try
        {
            LdapPerson ldapPerson = ldapPersonService.findByUserId("Some invalid User ID");
        }
        catch (NameNotFoundException nnfe)
        {
            Assert.assertNotNull(nnfe);
        }
    }

    /*
     * Only three of the four entries in the unversalUserIdList are valid
     * Universal User IDs. Therefore the result set should contain only three
     * LDAP Persons.
     */
    public void testFindByUniversalUserIdListOfString()
    {
        List<LdapPerson> universalUserIdListResultSet = ldapPersonService.findByUniversalUserId(universalUserIdList);
        Assert.assertTrue(universalUserIdListResultSet.size() == 3);
    }

    public void testFindByUserIdPublishedPerson()
    {
        LdapPerson personByUUID = ldapPersonService.findByUniversalUserId(chancellorsMailListing.getUcdPersonUUID());
        LdapPerson personByUserId = ldapPersonService.findByUserId(personByUUID.getUid().get(0));
        Assert.assertEquals(personByUUID.getUid(), personByUserId.getUid());
    }

    /*
     * If bound to LDAP anonymously, unpublished Persons should not be found. If
     * bound to LDAP using a special user account, unpublished Persons should be
     * found.
     */
    public void testFindByUserIdUnpublishedPerson()
    {
        // Round 1: Published
        LdapPerson unpublishedPersonResult1 = ldapPersonService.findByUserId("sljinks");
        System.out.println(unpublishedPersonResult1.getUcdPersonUUID());
        Assert.assertEquals(unpublishedPersonResult1.getUid(), unpublishedPerson.getUid());
        Assert.assertTrue(unpublishedPersonResult1.getEmployeeNumber() != null);
        Assert.assertTrue(unpublishedPersonResult1.getTitle() != null);
        Assert.assertTrue(unpublishedPersonResult1.getMail() != null);
        
        // Round 1: Unpublished
        NameNotFoundException nnfe1 = null;
        try
        {
            LdapPerson unpublishedPersonResultAnonymous1 = anonymousLdapPersonService.findByUserId("moslis");
        }
        catch (NameNotFoundException e)
        {
            nnfe1 = e;
        }
        Assert.assertNotNull(nnfe1);

        // Round 2: Published
        LdapPerson unpublishedPersonResult2 = ldapPersonService.findByUserId("philth");
        Assert.assertTrue(unpublishedPersonResult2.getEmployeeNumber() != null);
        Assert.assertTrue(unpublishedPersonResult2.getTitle() != null);
        Assert.assertTrue(unpublishedPersonResult2.getMail() != null);
        
        // Round 2: Unpublished
        NameNotFoundException nnfe2 = null;
        try
        {
            LdapPerson unpublishedPersonResultAnonymous2 = anonymousLdapPersonService.findByUserId("philth");
        }
        catch (NameNotFoundException e)
        {
            nnfe2 = e;
        }
        Assert.assertNotNull(nnfe2);
    }

    public void testFindByCriteria()
    {
        // Search for all persons in the same department who are staff
        Map<String, String> searchCriteriaDeptStaff = new HashMap<String, String>();
        searchCriteriaDeptStaff.put(LdapPersonConstants.PRIMARY_DEPARTMENT_CODE, "061000");
        searchCriteriaDeptStaff.put(LdapPersonConstants.AFFILIATION_TYPE_CODE, "staff");
        try
        {
            List<LdapPerson> searchCriteriaDeptStaffResultSet = ldapPersonService.findByCriteria(searchCriteriaDeptStaff);
            Assert.assertTrue(!searchCriteriaDeptStaffResultSet.isEmpty());
            Assert.assertTrue(searchCriteriaDeptStaffResultSet.size() >= 1);
        }
        catch (LimitExceededException lee)
        {
        }

        // Search for non-existent persons
        NameNotFoundException nnfe = null;
        List<LdapPerson> searchCriteriaInvalidPersonResultSet = null;
        Map<String, String> searchCriteriaInvalidPerson = new HashMap<String, String>();
        searchCriteriaInvalidPerson.put(LdapPersonConstants.FIRST_NAME, "dfsdfsagfsdfg");
        searchCriteriaInvalidPerson.put(LdapPersonConstants.LAST_NAME, "sdfgsdfgsdfg");
        try
        {
            searchCriteriaInvalidPersonResultSet = ldapPersonService.findByCriteria(searchCriteriaInvalidPerson);
        }
        catch (NameNotFoundException e)
        {
            nnfe = e;
        }
        Assert.assertNull(searchCriteriaInvalidPersonResultSet);
        Assert.assertNotNull(nnfe);
    }

    public void testFindByWildCardCriteria()
    {
        // Search for all persons whose e-mail address has some string in it
        Map<String, String> searchCriteriaEmail = new HashMap<String, String>();
        searchCriteriaEmail.put(LdapPersonConstants.EMAIL_ADDRESS, " lor@ucdavis.edu");
        boolean exactMatch = false;
        List<LdapPerson> searchCriteriaNameResultSet = ldapPersonService.findByCriteria(searchCriteriaEmail, exactMatch);
        Assert.assertTrue(!searchCriteriaNameResultSet.isEmpty());
        Assert.assertTrue(searchCriteriaNameResultSet.size() >= 1);
        for (LdapPerson ldapPerson : searchCriteriaNameResultSet)
        {
            Assert.assertNotNull(ldapPerson.getMail());
        }
    }


    public void testFindByLike()
    {
        // Search for all persons whose e-mail address has some string in it
        Map<String, String> searchCriteriaEmail = new HashMap<String, String>();
        searchCriteriaEmail.put(LdapPersonConstants.LAST_NAME, "Bra*");
        List<LdapPerson> searchCriteriaNameResultSet = ldapPersonService.findByCriteria(searchCriteriaEmail, LdapSearchType.Like);
        Assert.assertTrue(!searchCriteriaNameResultSet.isEmpty());
        Assert.assertTrue(searchCriteriaNameResultSet.size() >= 1);
        for (LdapPerson ldapPerson : searchCriteriaNameResultSet)
        {
            Assert.assertNotNull(ldapPerson.getSn());
        }
    }    
    public void testFindByDepartmentId()
    {
        // Search for all persons in the same department
        try
        {
            List<LdapPerson> searchCriteriaDeptResultSet = ldapPersonService.findByDepartmentId("061000");
            Assert.assertTrue(!searchCriteriaDeptResultSet.isEmpty());
            Assert.assertTrue(searchCriteriaDeptResultSet.size() >= 1);
        }
        catch (LimitExceededException lee)
        {
        }
    }

    /*
     * Convert a single String into a String array of one node
     */
    private String[] getSingleNodeStringArray(String attribute)
    {
        String[] attributeString = new String[1];
        attributeString[0] = attribute;
        return attributeString;
    }
    
    /*
     * Test the toString() implementation
     * 
     */
    public void testToString()
    {
    	LdapPerson ldapPerson = ldapPersonService.findByUserId("katehi");
    	Assert.assertNotNull(ldapPerson);
    	System.out.println(ldapPerson.toString());
    }
}
