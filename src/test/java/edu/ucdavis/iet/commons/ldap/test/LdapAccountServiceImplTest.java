package edu.ucdavis.iet.commons.ldap.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.ucdavis.iet.commons.ldap.domain.LdapAccount;
import edu.ucdavis.iet.commons.ldap.domain.LdapPerson;
import edu.ucdavis.iet.commons.ldap.domain.util.LdapPersonConstants;
import edu.ucdavis.iet.commons.ldap.service.LdapAccountService;
import edu.ucdavis.iet.commons.ldap.service.LdapPersonService;

import junit.framework.Assert;
import junit.framework.TestCase;

public class LdapAccountServiceImplTest extends TestCase
{
    private String[] configLocations = {"classpath*:edu/ucdavis/iet/commons/ldap/test/config/ldap-common.xml"
                                       ,"classpath*:edu/ucdavis/iet/commons/ldap/test/config/ldap-data.xml"
                                       ,"classpath*:edu/ucdavis/iet/commons/ldap/config/iet-commons-ldap.xml"};
    private ApplicationContext context = new ClassPathXmlApplicationContext(configLocations);
    private LdapPersonService ldapPersonService = (LdapPersonService)context.getBean("ldapPersonService");
    private LdapAccountService ldapAccountService = (LdapAccountService)context.getBean("ldapAccountService");
    List<LdapPerson> ldapPersonList;
    
    List<LdapAccount> ldapAccountFoundByUserIDNumber = null;
    
    protected void setUp() throws Exception
    {
        super.setUp();
        Map<String,String> searchCriteria = new HashMap<String,String>(1);
        searchCriteria.put(LdapPersonConstants.EMAIL_ADDRESS, "chancellor@ucdavis.edu");
        ldapPersonList = ldapPersonService.findByCriteria(searchCriteria);
    }

    public void testFindAccountsByUserID()
    { 
        for (LdapPerson ldapPerson : ldapPersonList)
        {
            List<LdapAccount> ldapAccounts = null;
            ldapAccounts = ldapAccountService.findAccountsByUserID(ldapPerson.getUid().get(0));
            Assert.assertNotNull(ldapAccounts);
            Assert.assertTrue(!ldapAccounts.isEmpty());
        }
    }

    public void testFindAccountsByUserIDNumber()
    {
        for (LdapPerson ldapPerson : ldapPersonList)
        {
            List<LdapAccount> ldapAccountsByUserId = ldapAccountService.findAccountsByUserID(ldapPerson.getUid().get(0));
            for (LdapAccount ldapAccount : ldapAccountsByUserId)
            {
                List<LdapAccount> ldapAccountsByUserIdNumber = null;
                ldapAccountsByUserIdNumber = ldapAccountService.findAccountsByUserIDNumber(ldapAccount.getUidNumber());
                Assert.assertNotNull(ldapAccountsByUserIdNumber);
                Assert.assertTrue(!ldapAccountsByUserIdNumber.isEmpty());
            } 
        }
    }

    public void testFindAccountsByUserIdNumberWithBlankUserIdNumber()
    {
        IllegalArgumentException iae = null;
        try
        {
            List<LdapAccount> ldapAccount = ldapAccountService.findAccountsByUserIDNumber(" ");
        }
        catch (IllegalArgumentException e)
        {
            iae = e;
        }
        Assert.assertNotNull(iae);
    }
    
    public void testFindByUserId()
    {
        for (LdapPerson ldapPerson : ldapPersonList)
        {
            String ldapAccountUserId = null;
            LdapAccount ldapAccount = ldapAccountService.findByUserId(ldapPerson.getUid().get(0), "IKRB");
            ldapAccountUserId = ldapAccount.getUid();
            Assert.assertTrue(StringUtils.isNotEmpty(ldapAccountUserId));
        }
    }

    public void testFindByUserIdWithBlankUserId()
    {
        for (LdapPerson ldapPerson : ldapPersonList)
        {
            IllegalArgumentException iae = null;
            try{
                LdapAccount ldapAccount = ldapAccountService.findByUserId(" ", "IKRB");
            }
            catch (IllegalArgumentException e)
            {
                iae = e;
            }
            Assert.assertNotNull(iae);
        }
    }
    
    public void testFindByUserIdNumber()
    {
        for (LdapPerson ldapPerson : ldapPersonList)
        {
            List<LdapAccount> ldapAccountsByUserId = ldapAccountService.findAccountsByUserID(ldapPerson.getUid().get(0));
            for (LdapAccount ldapAccount : ldapAccountsByUserId)
            {
                String ldapAccountUserIdNumber = null;
                LdapAccount ldapAccountsByUserIdNumber = ldapAccountService.findByUserIdNumber(ldapAccount.getUidNumber(), "IKRB");
                ldapAccountUserIdNumber = ldapAccountsByUserIdNumber.getUidNumber();
                Assert.assertTrue(StringUtils.isNotEmpty(ldapAccountUserIdNumber));                
            }
        }
    }
    
    /*
     * Test the toString() implementation
     * 
     */
    public void testToString()
    {
    	LdapAccount ldapAccount = ldapAccountService.findByUserId("katehi", "IKRB");
    	System.out.println(ldapAccount.toString());
    }
}