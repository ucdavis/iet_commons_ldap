package edu.ucdavis.iet.commons.ldap.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import junit.framework.Assert;
import junit.framework.TestCase;

import edu.ucdavis.iet.commons.ldap.domain.LdapMailListing;
import edu.ucdavis.iet.commons.ldap.domain.LdapPerson;
import edu.ucdavis.iet.commons.ldap.domain.util.LdapPersonConstants;
import edu.ucdavis.iet.commons.ldap.service.LdapMailListingService;

public class LdapMailListingServiceImplTest extends TestCase
{
    private String[] configLocations = {"classpath*:edu/ucdavis/iet/commons/ldap/test/config/ldap-common.xml"
                                       ,"classpath*:edu/ucdavis/iet/commons/ldap/test/config/ldap-data.xml"
                                       ,"classpath*:edu/ucdavis/iet/commons/ldap/config/iet-commons-ldap.xml"};
    private ApplicationContext context = new ClassPathXmlApplicationContext(configLocations);
    private LdapMailListingService ldapMailListingService = (LdapMailListingService) context.getBean("ldapMailListingService");
    private List<String> validUniversalUserIdList = new ArrayList<String>();
    private List<String> invalidUniversalUserIdList = new ArrayList<String>();

    protected void setUp() throws Exception
    {
        // Initialize a list of valid LDAP Persons
        validUniversalUserIdList.add("00002058");
        validUniversalUserIdList.add("00002059");
        validUniversalUserIdList.add("00002051");
        validUniversalUserIdList.add("00168762");
        validUniversalUserIdList.add("00022329");
        validUniversalUserIdList.add("00026137");
        validUniversalUserIdList.add("00009855");
        validUniversalUserIdList.add("00002544");
        validUniversalUserIdList.add("00002407");
        validUniversalUserIdList.add("00002509");
        validUniversalUserIdList.add("00531553");
        validUniversalUserIdList.add("00004303");
        validUniversalUserIdList.add("00003003");
        validUniversalUserIdList.add("00003238");
        validUniversalUserIdList.add("00002759");
        
        // Initialize a list of invalid LDAP Persons 
        invalidUniversalUserIdList.add("00002060");
        invalidUniversalUserIdList.add("00025899");
        invalidUniversalUserIdList.add("00366911");
        invalidUniversalUserIdList.add("00374421");
        invalidUniversalUserIdList.add("00212266");
        invalidUniversalUserIdList.add("sdfgsdgsdfgsdfg");
        
        
    }

    /*
     * Find each primary LDAP Mail Listing for each valid Universal User ID in a list.
     */
    public void testFindPrimaryMailListings()
    {
        //for (String ucdPersonUUID : validUniversalUserIdList)
        //{
            LdapMailListing primaryLdapMailListing = ldapMailListingService.findPrimaryMailListing("00031208");//ucdPersonUUID);
            System.out.println(primaryLdapMailListing.getMail());
            Assert.assertNotNull(primaryLdapMailListing.getMail());
        //}
    }

    /*
     * Find each primary LDAP Mail Listing for each invalid Universal User ID in a list.
     */
    public void testFindPrimaryMailListingsByInvalidUniversalUserId()
    {
        for (String ucdPersonUUID : invalidUniversalUserIdList)
        {
            LdapMailListing primaryLdapMailListing = ldapMailListingService.findPrimaryMailListing(ucdPersonUUID);
            Assert.assertNull(primaryLdapMailListing);
        }
    }
    
    /*
     * Find all LDAP Mail Listings for each valid Universal User ID in a list.
     */
    public void testFindAllMailListings()
    {
        for (String ucdPersonUUID : validUniversalUserIdList)
        {
            List<LdapMailListing> allMailListings = ldapMailListingService.findMailListings(ucdPersonUUID);
            for (LdapMailListing mailListing : allMailListings)
            {
                Assert.assertNotNull(mailListing.getMail());
            }
        }
    }
 
    /*
     * Find zero LDAP Mail Listings for each invalid Universal User ID in a list.
     */
    public void testFindAllMailListingsByInvalidUniversalUserId()
    {
        for (String ucdPersonUUID : invalidUniversalUserIdList)
        {
            List<LdapMailListing> allMailListings = ldapMailListingService.findMailListings(ucdPersonUUID);
            Assert.assertEquals(allMailListings.size(), 0);
        }
    }
    
    /*
     * Find all LDAP Mail Listings by e-mail addresses.
     */
    public void testFindByCriteriaEmailAddresses()
    {
        Map<String, String> searchCriteriaEmail = new HashMap<String, String>();
        searchCriteriaEmail.put(LdapPersonConstants.EMAIL_ADDRESS, "chancellor@ucdavis.edu");
        List<LdapMailListing> allMailListings = ldapMailListingService.findByCriteria(searchCriteriaEmail);
        Assert.assertNotNull(allMailListings);
    }
    
    /*
     * Test the toString() implementation
     * 
     */
    public void testToString()
    {
    	List<LdapMailListing> ldapMailListingList = ldapMailListingService.findByEmailAddress("chancellor@ucdavis.edu");
    	for (LdapMailListing ldapMailListing : ldapMailListingList)
    	{
    		System.out.println(ldapMailListing.toString());
    	}
    	
    }
}
